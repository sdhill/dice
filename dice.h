
#pragma once

#include <random>

namespace dice {

template<size_t num_sides, int min_val = 1>
class die
{
    static_assert( 2 <= num_sides, "die must have a minimum of 2 sides");

    public:
    die() : dist(min_val, min_val + num_sides - 1 )
    {
        std::random_device r;  // seed the random number generator
        rnd.seed(r());
    }

    /** Roll the die and return the result.
     */
    inline int operator()() { return roll(); }
    inline int roll() { return dist(rnd); }

    /** Roll the die n times and return the total of the rolls.
     */
    int total_of_n_rolls(size_t n)
    {
        int result = 0;
        while( n-- ) 
            result += roll();
        return result;
    }

    /** Roll the die n times and write the result of each roll 
     * to the iterator.
     */
    template<class OutputIter>
    void roll_n_times(size_t n, OutputIter out)
    {
        while( n-- )
            *out++ = roll();
    }

    /** Fill the range [begin, end) with die rolls.
     */
    template<class OutputIter>
    void roll_range(OutputIter begin, OutputIter end)
    {
        while( begin < end )
            *begin++ = roll();
    }

    private:
    std::uniform_int_distribution<int> dist;
    std::default_random_engine rnd;
};

// mnemonics for some common sizes of dice.
//
using D4  = die<4>;
using D6  = die<6>;
using D8  = die<8>;
using D10 = die<10>;
using D12 = die<12>;
using D20 = die<20>;

};

