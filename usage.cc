
#include <array>
#include <vector>
#include <cassert>

#include "dice.h"

bool in_d6_range(int val) { return (1 <= val && val <= 6); }

int main()
{
    // Create the die. dice::D6 is equivalent to dice::die<6,1>.
    // die is a template which takes the parameters num_sides, and
    // min_val. num_sides is unsigned and must be at least 2.
    //
    dice::D6 die;

    // roll
    // Rolls the die and returns the result. A die roll is in the 
    // range [min_val, min_val + num_sides - 1].
    //
    for( size_t i = 0; i < 100; ++i )
        assert(in_d6_range(die.roll()));


    // operator()
    // die can be used as a functor and is equivalent to calling 
    // roll(). This allows die to be used in algorithms.
    //
    std::array<int, 6> rolls;
    rolls.fill(-1);
    std::generate(rolls.begin(), rolls.end(), die);
    for( const int r : rolls )
        assert(in_d6_range(r));


    // total_of_n_rolls
    // Roll the die n times and return the total of the rolls. 
    // If n is 0 then 0 is returned.
    //
    // NOTE: n is unsigned
    //
    for( int i = 0; i < 10; ++i )
    {
        int tot = die.total_of_n_rolls(i);
        assert( 1 * i <= tot && tot <= 6 * i);
    }


    // roll_n_times
    // Rolls the die n times and writes the result of each
    // roll to the iterator. 
    //
    // NOTE: n is unsigned
    //
    std::vector<int> roll_vec;
    die.roll_n_times(15, std::back_inserter(roll_vec));
    assert(15 == roll_vec.size());
    for( const int r : roll_vec )
        assert(in_d6_range(r));


    // roll_range
    // Fills the iterator range [begin, end) with die rolls
    //
    rolls.fill(-1);
    die.roll_range(rolls.begin(), rolls.end());
    for( const auto r : rolls )
        assert(in_d6_range(r));
    
}
